const restApi = 'http://127.0.0.1:8080';
export const environment = {
  production: false,
  restApi: restApi,
  timeout: 30000
};