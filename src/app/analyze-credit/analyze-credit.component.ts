import { LoanDTO } from './../domain/loanDTO';
import { Component, OnInit } from '@angular/core';
import { ClientService } from '../service/client.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientDTO } from '../domain/ClientDTO';
import { RiskEnum } from '../enums/RiskEnum';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';

@Component({
  selector: 'app-analyze-credit',
  templateUrl: './analyze-credit.component.html',
  styleUrls: ['./analyze-credit.component.css']
})
export class AnalyzeCreditComponent implements OnInit {

  LoanDTO:LoanDTO= new LoanDTO();

  returnLoanDTO:any;

  id: number;
  client: ClientDTO;

  
  submitted=false;
  
  constructor(
    private route: ActivatedRoute,
    private clientService:ClientService,
    private router:Router) { }

  ngOnInit() {
    this.client= new ClientDTO();
    this.id=this.route.snapshot.params['id'];

    this.clientService.getClient(this.id)
    .subscribe(data=> {
      this.client=data;
      
    }, error => console.log(error))
  }

  simulation() {
    this.LoanDTO.clientRisk=this.getRisk(this.client.riskEnum)
    this.clientService.simulation(this.LoanDTO)
      .subscribe(data => this.returnLoanDTO=data
      , error => console.log(error));
    this.LoanDTO = new LoanDTO();
    
  }

  onSubmit() {
    this.submitted = true;
    this.simulation()
  }

  getRisk(risk:string){
    if(risk="A")
    return RiskEnum.RisckA

    if(risk="B")
    return RiskEnum.RisckB

    if(risk="C")
    return RiskEnum.RiskC;
  

  }

}
