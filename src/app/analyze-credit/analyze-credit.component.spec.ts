import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyzeCreditComponent } from './analyze-credit.component';

describe('AnalyzeCreditComponent', () => {
  let component: AnalyzeCreditComponent;
  let fixture: ComponentFixture<AnalyzeCreditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyzeCreditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyzeCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
