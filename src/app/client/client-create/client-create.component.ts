import { AddressDTO } from './../../domain/AddressDTO';
import { Router } from '@angular/router';
import { ClientService } from './../../service/client.service';
import { ClientDTO } from './../../domain/ClientDTO';
import { Component, OnInit } from '@angular/core';
import { RiskEnum } from 'src/app/enums/RiskEnum';

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css']
})
export class ClientCreateComponent implements OnInit {

  client:ClientDTO= new ClientDTO();
  address:AddressDTO= new AddressDTO();
  submitted=false;
  addressList: Array<AddressDTO> = [];

  risk:string="";

  constructor(
    private clientService:ClientService,
    private router:Router
  ) { 

  }

  ngOnInit() {
    this.risk="";
  }
  newClient(): void {
    this.submitted = false;
    this.client = new ClientDTO();
    this.address= new AddressDTO();
  }

  save() {
    this.addressList[0]=this.address;
    this.client.address=this.addressList;
    this.risk=this.getRisk(this.risk);
    this.clientService.createClient(this.client)
      .subscribe(data => 
        console.log(data),
       error => console.log(error));
    this.client = new ClientDTO();
    this.gotoList();
  }

  getRisk(risk:string){
    if(risk="A")
    return RiskEnum.RisckA

    if(risk="B")
    return RiskEnum.RisckB

    if(risk="C")
    return RiskEnum.RiskC;
  

  }
  onSubmit() {
    this.submitted = true;
    this.save();  
  }

  gotoList() {
    this.router.navigate(['/clients']);
  }

  showRisk(e) {
    console.log(e)
    this.clientService.getAssessrisk(e).subscribe(data=> {
      console.log(data)
      this.risk = data.risk;
    })
    }


}
