import { ClientService } from './../../service/client.service';
import { ClientDTO } from './../../domain/ClientDTO';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  clients: Observable<ClientDTO[]>;

  constructor(
    private ClientService:ClientService,
    private router:Router
  ) { }

  ngOnInit() {
    this.reloadData();
  }
  
  reloadData(){
    this.clients= this.ClientService.getClientList();
  }

  delete(id: number) {
    this.ClientService.deleteClient(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  update(id: number){
    this.router.navigate(['update', id]);
  }

  simulation(id: number){
    this.router.navigate(['analyze', id]);
  }

}
