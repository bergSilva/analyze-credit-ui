import { AddressDTO } from './../../domain/AddressDTO';
import { ClientService } from './../../service/client.service';
import { Component, OnInit } from '@angular/core';
import { ClientDTO } from 'src/app/domain/ClientDTO';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-client-update',
  templateUrl: './client-update.component.html',
  styleUrls: ['./client-update.component.css']
})
export class ClientUpdateComponent implements OnInit {

  id: number;
  client: ClientDTO;
  address: AddressDTO;
  addressList: Array<AddressDTO> = [];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private clientService: ClientService) { }

  ngOnInit() {
    this.client= new ClientDTO();
    this.address=new AddressDTO();
    this.id=this.route.snapshot.params['id'];

    this.clientService.getClient(this.id)
    .subscribe(data=> {
      this.client=data;
      this.address= this.client.address[0];
      
    }, error => console.log(error))
  }

  updateClient() {
    this.client.address.fill;                                                         
    this.addressList[0]=this.address;
    this.client.address=this.addressList;
    console.log(this.client)
    this.clientService.updateClient(this.id, this.client)
      .subscribe(data => console.log(data), error => console.log(error));
    this.client = new ClientDTO();
    this.gotoList();
  }

  onSubmit() {
    this.updateClient();    
  }

  gotoList() {
    this.router.navigate(['/clients']);
  }

}
