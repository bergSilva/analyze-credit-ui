import { AddressDTO } from './AddressDTO';
import { RiskEnum } from './../enums/RiskEnum';
export class ClientDTO{
    id:number;
    name:string;
    redimentoMensal:number;
    riskEnum:RiskEnum;
    address:Array<AddressDTO>=[];

    
}