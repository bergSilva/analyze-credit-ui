export class LoanDTO{
    clientRisk:string;
    valueLoan:number
    amountMonths:number
    interestRate:number
    amount:number;
}