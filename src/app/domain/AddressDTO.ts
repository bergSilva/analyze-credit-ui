export class AddressDTO{
    id:number;
    street:string;
    number:string;
    zipCode:string;
}