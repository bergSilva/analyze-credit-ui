import { environment } from './../../environments/environment';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ClientService {
  
  baseUrl= environment.restApi;


  constructor(private http:HttpClient) { }

  getClient(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/client/${id}`);
  }

  createClient(client: Object): Observable<Object> {
    console.log(client);
    return this.http.post(`${this.baseUrl}/client`, client);
  }

  updateClient(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/client/${id}`, value);
  }

  deleteClient(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/client/${id}`, { responseType: 'text' });
  }

  getClientList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/client`);
  }

  getAssessrisk(renda:number): Observable<any> {
    return this.http.get(`${this.baseUrl}/client/assessrisk/${renda}`);
  }

  simulation(loan: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}/client/simulation`, loan);
  }


}
