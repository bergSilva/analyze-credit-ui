import { AnalyzeCreditComponent } from './analyze-credit/analyze-credit.component';
import { ClientUpdateComponent } from './client/client-update/client-update.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientListComponent } from './client/client-list/client-list.component';
import { ClientCreateComponent } from './client/client-create/client-create.component';


const routes: Routes = [
  { path: '', redirectTo: 'client', pathMatch: 'full' },
  { path: 'clients', component: ClientListComponent },
  { path: 'add', component: ClientCreateComponent },
  { path: 'update/:id', component: ClientUpdateComponent },
  { path: 'analyze/:id', component:AnalyzeCreditComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
